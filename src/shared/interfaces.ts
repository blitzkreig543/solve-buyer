export interface UserCredentials {
    email: string;
    password: string;
}

export interface Predicate<T> {
    (item: T): boolean;
}

export interface ValidationResult {
    [key: string]: boolean;
}

export interface IJob {
    key?: string;
    dateCreated: string;
    jobStatus: string;
    user: IUser;
    quotations: number;
    jobDate: string;
    jobTime: string;
    pincode: number;
    city: string;
    jobCategoryId?: string;
    chefGuestsNumber?: number;
    chefEventType?: string;
    cleanNoRooms?: number;
    cleanAddRooms?: string;
    washNoClothes?: string;
    washService?: string;
    recycleType?: string;
    paintNoWalls?: number;
    paintMaterials?: string;
    fAssemblyNoUnits?: number;
    fMoveNoUnits?: number;
    fMoveDestAddress?: string;
    customJob?: string;
    quotation?: IQuotation;
}

export interface IQuotation {
    key?: string;
    jobKey: string;
    price: number;
    message: string;
    user: IUser;
    dateCreated: string;
    status?: string;
}

export interface IUser {
    uid: string;
    username: string;
}

export interface IBJob {
    key: string;
    dateCreated: string;
    jobStatus: string;
    user: IUser;
    quotations: number;
    jobDate: string;
    jobTime: string;
    pincode: number;
    city: string;
    jobCategoryId?: string;
    chefGuestsNumber?: number;
    chefEventType?: string;
    cleanNoRooms?: number;
    cleanAddRooms?: string;
    washNoClothes?: string;
    washService?: string;
    recycleType?: string;
    paintNoWalls?: number;
    paintMaterials?: string;
    fAssemblyNoUnits?: number;
    fMoveNoUnits?: number;
    fMoveDestAddress?: string;
    customJob?: string;
    quotation: IQuotation;
}

export interface IRating {
    key?: string;
    sellerId?: string;
    job?: IJob;
    pricing: string;
    quality: string;
    punctuality: string;
    responsiveness: string;
    professionalism: string;

}

export interface IMessage {
    quotationKey?: string;
    key?: string;
    message: string;
    user: IUser;
    dateCreated: string;
}
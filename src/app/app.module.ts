import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { LoginPage } from '../pages/login/login';
import { ChatsPage } from '../pages/chats/chats';
import { ChatPage } from '../pages/chat/chat';
import { JobsPage } from '../pages/jobs/jobs';
import { RatingPage } from '../pages/rating/rating';
import { ProfileImageUploadPage } from '../pages/profile-image-upload/profile-image-upload';
import { BookedJobsPage } from '../pages/booked-jobs/booked-jobs';
import { SellerProfilePage } from '../pages/seller-profile/seller-profile';
import { CreateAssemblyJobPage } from '../pages/create-assembly-job/create-assembly-job';
import { CreateCleaningJobPage } from '../pages/create-cleaning-job/create-cleaning-job';
import { CreateCookingJobPage } from '../pages/create-cooking-job/create-cooking-job';
import { CreateCustomJobPage } from '../pages/create-custom-job/create-custom-job';
import { CreateMovingJobPage } from '../pages/create-moving-job/create-moving-job';
import { CreatePaintingJobPage } from '../pages/create-painting-job/create-painting-job';
import { CreateRecyclingJobPage } from '../pages/create-recycling-job/create-recycling-job';
import { CreateWashingJobPage } from '../pages/create-washing-job/create-washing-job';
import { ProfilePage } from '../pages/profile/profile';
import { FeedbackPage } from '../pages/feedback/feedback';
import { UserProfilePage } from '../pages/user-profile/user-profile';

import { JobComponent } from '../shared/components/job.component';
import { BookedJobComponent } from '../shared/components/bookedjob.component';
import { UserAvatarComponent } from '../shared/components/user-avatar.component';
import { RatingsComponent } from '../shared/components/ratings.component'

import { UserData } from '../providers/user-data';
import { APP_PROVIDERS } from '../providers/app.providers';


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    TutorialPage,
    SignUpPage,
    LoginPage,
    CreateAssemblyJobPage,
    CreateCleaningJobPage,
    CreateCookingJobPage,
    CreateCustomJobPage,
    CreateMovingJobPage,
    CreatePaintingJobPage,
    CreateRecyclingJobPage,
    CreateWashingJobPage,
    ChatsPage,
    ChatPage,
    JobsPage,
    RatingPage,
    BookedJobsPage,
    SellerProfilePage,
    ProfileImageUploadPage,
    JobComponent,
    BookedJobComponent,
    UserAvatarComponent,
    RatingsComponent,
    ProfilePage,
    UserProfilePage,
    FeedbackPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    TutorialPage,
    SignUpPage,
    LoginPage,
    CreateAssemblyJobPage,
    CreateCleaningJobPage,
    CreateCookingJobPage,
    CreateCustomJobPage,
    CreateMovingJobPage,
    CreatePaintingJobPage,
    CreateRecyclingJobPage,
    CreateWashingJobPage,
    ChatsPage,
    ChatPage,
    JobsPage,
    RatingPage,
    BookedJobsPage,
    SellerProfilePage,
    ProfileImageUploadPage,
    ProfilePage,
    UserProfilePage,
    FeedbackPage
  ],
  providers: [UserData, Storage, APP_PROVIDERS]
})
export class AppModule {}

import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IJob } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';
import { ItemsService } from '../../shared/services/items.service';
import { MappingsService } from '../../shared/services/mappings.service';
import { JobComponent } from '../../shared/components/job.component';

/*
  Generated class for the BookedJobs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-booked-jobs',
  templateUrl: 'booked-jobs.html'
})
export class BookedJobsPage implements OnInit {
  public jobs: Array<IJob> = [];
  public newJobs: Array<IJob> = [];
  public loading: boolean = true;

  constructor(public navCtrl: NavController,
    public authService: AuthService,
    public dataService: DataService,
    public mappingsService: MappingsService,
    public itemsService: ItemsService) { }

  ionViewDidLoad() {
    console.log('Hello JobsPage Page');
  }
  ngOnInit() {
    var self = this;
    self.loadJobs(true);
    }

  loadJobs(fromStart: boolean) {
    var self = this;
    if (fromStart)
    self.loading = true;
    self.jobs = [];
    this.dataService.getUserJobsRef().child(self.authService.getLoggedInUser().uid).orderByChild('jobStatus').equalTo('booked').once('value', function (snapshot) {
      self.itemsService.reversedItems<IJob>(self.mappingsService.getBookedJobs(snapshot)).forEach(function (job) {
        self.jobs.push(job);
        console.log(job);
        self.loading = false;
      });
    });
  }

    reloadJobs(refresher) {
      this.loadJobs(true);
      refresher.complete();
  }

}
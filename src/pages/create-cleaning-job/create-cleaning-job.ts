import { Component, OnInit } from '@angular/core';
import { NavController, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { IJob } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';


/*
  Generated class for the CreateCleaningJob page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-create-cleaning-job',
  templateUrl: 'create-cleaning-job.html'
})
export class CreateCleaningJobPage implements OnInit {

  createJobForm: FormGroup;
  cleanNoRooms: AbstractControl;
  cleanAddRooms: AbstractControl;
  jobDate: AbstractControl;
  jobTime: AbstractControl;
  pincode: AbstractControl;
  city: AbstractControl;
  jobCategoryId: string = 'cleaning';
  status: string = 'open';

  constructor(public navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private viewCtrl: ViewController,
    private fb: FormBuilder,
    private dataService: DataService,
    private authService: AuthService,
    private toastCtrl: ToastController) { }

  ionViewDidLoad() {
    console.log('Hello CreateCleaningJobPage Page');
  }

  ngOnInit() {
    console.log('creating cleaning job');
    this.createJobForm = this.fb.group({
      'cleanNoRooms': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
      'cleanAddRooms': [''],
      'jobTime': [new Date().toTimeString().slice(10), Validators.compose([Validators.required])],
      'jobDate': [new Date().toISOString().slice(0, 10), Validators.compose([Validators.required])],
      'pincode': ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(6)])],
      'city': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
    });

    this.cleanNoRooms = this.createJobForm.controls['cleanNoRooms'];
    this.cleanAddRooms = this.createJobForm.controls['cleanAddRooms'];
    this.jobDate = this.createJobForm.controls['jobDate'];
    this.jobTime = this.createJobForm.controls['jobTime'];
    this.pincode = this.createJobForm.controls['pincode'];
    this.city = this.createJobForm.controls['city'];
  }

  cancelNewJob() {
    this.viewCtrl.dismiss();
  }

  onSubmit(job: any): void {
    var self = this;
    if (this.createJobForm.valid) {

      let loader = this.loadingCtrl.create({
        content: 'Posting Cleaning Job',
        dismissOnPageChange: true
      });

      loader.present();

      let uid = self.authService.getLoggedInUser().uid;
      let category = this.jobCategoryId;
      let status = this.status;

      self.dataService.getUsername(uid).then(function (snapshot) {
        let username = snapshot.val();

        let newJob: IJob = {
          key: null,
          user: { uid: uid, username: username },
          dateCreated: new Date().toString(),
          jobDate: job.jobDate,
          jobTime: job.jobTime,
          quotations: null,
          pincode: job.pincode,
          city: job.city,
          cleanNoRooms: job.cleanNoRooms,
          cleanAddRooms: job.cleanAddRooms,
          jobCategoryId: category,
          jobStatus: status
        };

        self.dataService.submitJob(newJob, uid, category)
          .then(function (snapshot) {
            loader.dismiss()
              .then(() => {
                self.viewCtrl.dismiss({
                  newjob: newJob,
                  uid: uid,
                  category: category
                });
              }).then(() => {
                let toast = self.toastCtrl.create({
                  message: 'Job Posted Succesfully',
                  duration: 3000,
                  position: 'top'
                });
                toast.present();
              });
          }, function (error) {
            console.error(error);
            loader.dismiss();
          });
      });
    }
  }

}
import { Component } from '@angular/core';
import { NavController, Events, Tabs } from 'ionic-angular';

import { HomePage } from '../home/home';
import { ChatsPage } from '../chats/chats';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = ChatsPage;

    public newChats: string = '';

  constructor(private events: Events) {

  }
    ngOnInit() {
    this.startListening();
  }

  startListening() {
    var self = this;
    self.events.subscribe('chat:created', (jobData) => {
      if (self.newChats === '') {
        self.newChats = '1';
      } else {
        self.newChats = (+self.newChats + 1).toString();
      }
    });

    self.events.subscribe('chats:viewed', (jobData) => {
      self.newChats = '';
    });
  }
}

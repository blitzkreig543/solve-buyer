import { Component, EventEmitter, OnInit, OnDestroy, Input, Output } from '@angular/core';

import { NavController } from 'ionic-angular';

import { IJob } from '../interfaces';
import { UserAvatarComponent } from '../../shared/components/user-avatar.component';
import { DataService } from '../services/data.service';
import { RatingPage } from'../../pages/rating/rating';

@Component({
    selector: 'booked-job',
    templateUrl: 'bookedjob.component.html',
})

export class BookedJobComponent {
    @Input() job: IJob;
    @Output() onViewQuotations = new EventEmitter<string>();

    constructor(private dataService: DataService,
    private navCtrl: NavController) { }

    gotoRating(jobKey: string, sellerId: string) {
        this.navCtrl.push(RatingPage, {jobKey: jobKey, sellerId: sellerId});
        console.log('going to ratings page' + jobKey, 'seller id is ' + sellerId);
    }
}
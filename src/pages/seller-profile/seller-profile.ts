import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the SellerProfile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-seller-profile',
  templateUrl: 'seller-profile.html'
})
export class SellerProfilePage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello SellerProfilePage Page');
  }

}

import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { CreateAssemblyJobPage } from'../create-assembly-job/create-assembly-job';
import { CreateCleaningJobPage } from'../create-cleaning-job/create-cleaning-job';
import { CreateCookingJobPage } from'../create-cooking-job/create-cooking-job';
import { CreateCustomJobPage } from'../create-custom-job/create-custom-job';
import { CreateMovingJobPage } from'../create-moving-job/create-moving-job';
import { CreatePaintingJobPage } from'../create-painting-job/create-painting-job';
import { CreateRecyclingJobPage } from'../create-recycling-job/create-recycling-job';
import { CreateWashingJobPage } from'../create-washing-job/create-washing-job';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  gotoAssemblyPage() {
    this.navCtrl.push(CreateAssemblyJobPage);
    console.log('creating Assembly Job');
  }

  gotoCleaningPage() {
    this.navCtrl.push(CreateCleaningJobPage);
    console.log('creating Cleaning Job');
  }

  gotoCookingPage() {
    this.navCtrl.push(CreateCookingJobPage);
    console.log('creating Cooking Job');
  }

  gotoCustomPage() {
    this.navCtrl.push(CreateCustomJobPage);
    console.log('creating Custom Job');
  }

  gotoMovingPage() {
    this.navCtrl.push(CreateMovingJobPage);
    console.log('creating Moving Job');
  }

  gotoPaintingPage() {
    this.navCtrl.push(CreatePaintingJobPage);
    console.log('creating Painting Job');
  }

  gotoRecyclingPage() {
    this.navCtrl.push(CreateRecyclingJobPage);
    console.log('creating Recycling Job');
  }

  gotoWashingPage() {
    this.navCtrl.push(CreateWashingJobPage);
    console.log('creating Washing Job');
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IJob } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';
import { ItemsService } from '../../shared/services/items.service';
import { MappingsService } from '../../shared/services/mappings.service';
import { JobComponent } from '../../shared/components/job.component';

import { ChatsPage } from '../chats/chats';

/*
  Generated class for the Jobs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-jobs',
  templateUrl: 'jobs.html'
})
export class JobsPage implements OnInit {
  public jobs: Array<IJob> = [];
  public newJobs: Array<IJob> = [];
  public loading: boolean = true;

  constructor(public navCtrl: NavController,
    public authService: AuthService,
    public dataService: DataService,
    public mappingsService: MappingsService,
    public itemsService: ItemsService) { }

  ionViewDidLoad() {
    console.log('Hello JobsPage Page');
  }
  ngOnInit() {
    var self = this;
    self.loadJobs(true);
  }

  gotoChats() {
    this.navCtrl.push(ChatsPage);
  }

  loadJobs(fromStart: boolean) {
    var self = this;
    if (fromStart)
      self.loading = true;
    self.jobs = [];
    this.dataService.getUserJobsRef().child(self.authService.getLoggedInUser().uid).orderByChild('jobStatus').equalTo('open').once('value', function (snapshot) {
      self.itemsService.reversedItems<IJob>(self.mappingsService.getJobs(snapshot)).forEach(function (job) {
        self.jobs.push(job);
        console.log(job);
        self.loading = false;
      });
    });
  }

  reloadJobs(refresher) {
    this.loadJobs(true);
    refresher.complete();
  }

}

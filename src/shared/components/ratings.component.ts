import { Component, EventEmitter, OnInit, OnDestroy, Input, Output } from '@angular/core';

import { IRating } from '../interfaces';
import { UserAvatarComponent } from '../../shared/components/user-avatar.component';
import { DataService } from '../services/data.service';

@Component({
    selector: 'rating',
    templateUrl: 'ratings.component.html',
})

export class RatingsComponent {
    @Input() rating: IRating;
    @Output() onViewQuotations = new EventEmitter<string>();

    constructor(private dataService: DataService,
    ) { }
}
import { Component, OnInit } from '@angular/core';
import { NavController, Modal, ViewController, LoadingController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { IQuotation, IUser, IRating } from '../../shared/interfaces';
import { AuthService } from '../../shared/services/auth.service';
import { DataService } from '../../shared/services/data.service';
/*
  Generated class for the Rating page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-rating',
  templateUrl: 'rating.html'
})
export class RatingPage implements OnInit {
  createRatingForm: FormGroup;
  jobKey: string;
  sellerId: string;
  pricing: AbstractControl;
  quality: AbstractControl;
  punctuality: AbstractControl;
  responsiveness: AbstractControl;
  professionalism: AbstractControl;

  constructor(public navCtrl: NavController,
    private navParams: NavParams,
    private loadingCtrl: LoadingController,
    private viewCtrl: ViewController,
    private fb: FormBuilder,
    private authService: AuthService,
    private dataService: DataService) { }

  ionViewDidLoad() {
    console.log('Hello RatingPage Page');
  }

  ngOnInit() {
    var self = this;
    self.jobKey = self.navParams.get('jobKey');
    self.sellerId = self.navParams.get('sellerId');
    console.log('ratings page ' + self.jobKey, 'seller id' + self.sellerId);
    this.createRatingForm = this.fb.group({
      'pricing': ['', Validators.compose([Validators.required])],
      'quality': ['', Validators.compose([Validators.required])],
      'punctuality': ['', Validators.compose([Validators.required])],
      'responsiveness': ['', Validators.compose([Validators.required])],
      'professionalism': ['', Validators.compose([Validators.required])]
    });

    this.pricing = this.createRatingForm.controls['pricing'];
    this.quality = this.createRatingForm.controls['quality'];
    this.punctuality = this.createRatingForm.controls['punctuality'];
    this.responsiveness = this.createRatingForm.controls['responsiveness'];
    this.professionalism = this.createRatingForm.controls['professionalism'];
  }
  onSubmit(rating: any): void {
    var self = this;
    if (this.createRatingForm.valid) {

      let loader = this.loadingCtrl.create({
        content: 'Posting Rating...',
        dismissOnPageChange: true
      });

      loader.present();

      let uid = self.authService.getLoggedInUser().uid;
      let jobKey = this.jobKey;

      self.dataService.getUsername(uid).then(function (snapshot) {
        let username = snapshot.val();

        let newRating: IRating = {
          key: null,
          sellerId: self.sellerId,
          pricing: rating.pricing,
          quality: rating.quality,
          punctuality: rating.punctuality,
          responsiveness: rating.responsiveness,
          professionalism: rating.professionalism
        };

        self.dataService.submitRating(newRating, jobKey).then(function (snapshot) {
          loader.dismiss().then(() => {
            self.viewCtrl.dismiss({
              rating: newRating,
              jobKey: jobKey
            });
          });
        }, function (error) {
          console.log(error);
          loader.dismiss();
        });
      });
    }
  }

}

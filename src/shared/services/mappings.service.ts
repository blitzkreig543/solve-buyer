import { Injectable } from '@angular/core';
import { IJob, IQuotation, IBJob, IMessage, IRating } from '../interfaces';
import { ItemsService } from '../services/items.service';

@Injectable()
export class MappingsService {

    constructor(private itemsService: ItemsService) { }

    getJobs(snapshot: any): Array<IJob> {
        let jobs: Array<IJob> = [];
        if (snapshot.val() == null)
            return jobs;

        let list = snapshot.val();

        Object.keys(snapshot.val()).map((key: any) => {
            let job: any = list[key];
            jobs.push({
                key: key,
                jobCategoryId: job.jobCategoryId,
                dateCreated: job.dateCreated,
                user: {
                    uid: job.user.uid,
                    username: job.user.username
                },
                quotations: job.quotations == null ? 0 : job.quotations,
                jobDate: job.jobDate,
                jobTime: job.jobTime,
                pincode: job.pincode,
                jobStatus: job.jobStatus,
                city: job.city,
                chefGuestsNumber: job.chefGuestsNumber,
                chefEventType: job.eventType,
                cleanNoRooms: job.cleanNoRooms,
                cleanAddRooms: job.cleanAddRooms,
                customJob: job.customJob,
                washNoClothes: job.washNoClothes,
                washService: job.washService,
                recycleType: job.recycleType,
                paintNoWalls: job.paintNoWalls,
                paintMaterials: job.paintMaterials,
                fAssemblyNoUnits: job.fAssemblyNoUnits,
                fMoveNoUnits: job.fMoveNoUnits,
                fMoveDestAddress: job.fMoveDestAddress
            });
        });
        return jobs;
    }

    getJob(snapshot: any, key: string): IJob {

        let job: IJob = {
            key: key,
            jobCategoryId: snapshot.jobCategoryId,
            dateCreated: snapshot.dateCreated,
            user: snapshot.user,
            quotations: snapshot.quotations == null ? 0 : snapshot.quotations,
            jobDate: snapshot.jobDate,
            jobTime: snapshot.jobTime,
            jobStatus: snapshot.jobStatus,
            pincode: snapshot.pincode,
            city: snapshot.city,
            chefGuestsNumber: snapshot.chefGuestsNumber,
            chefEventType: snapshot.chefEventType,
            cleanNoRooms: snapshot.cleanNoRooms,
            cleanAddRooms: snapshot.cleanAddRooms,
            customJob: snapshot.customJob,
            washNoClothes: snapshot.washNoClothes,
            washService: snapshot.washService,
            recycleType: snapshot.recycleType,
            paintNoWalls: snapshot.paintNoWalls,
            paintMaterials: snapshot.paintMaterials,
            fAssemblyNoUnits: snapshot.fAssemblyNoUnits,
            fMoveNoUnits: snapshot.fMoveNoUnits,
            fMoveDestAddress: snapshot.fMoveDestAddress
        };
        return job;
    }

    getQuotations(snapshot: any): Array<IQuotation> {
        let quotations: Array<IQuotation> = [];
        if (snapshot.val() == null)
            return quotations;

        let list = snapshot.val();

        Object.keys(snapshot.val()).map((key: any) => {
            let quotation: any = list[key];
            quotations.push({
                key: key,
                message: quotation.message,
                jobKey: quotation.jobKey,
                price: quotation.price,
                user: quotation.user,
                dateCreated: quotation.dateCreated
            });
        });

        return quotations;
    }

    getQuotation(snapshot: any, key: string): IQuotation {

        let quotation: IQuotation = {
            key: key,
            message: snapshot.message,
            dateCreated: snapshot.dateCreated,
            user: snapshot.user,
            price: snapshot.price,
            jobKey: snapshot.jobKey,
        };

        return quotation;
    }

    getBookedJobs(snapshot: any): Array<IJob> {
        let jobs: Array<IJob> = [];
        if (snapshot.val() == null)
            return jobs;

        let list = snapshot.val();

        Object.keys(snapshot.val()).map((key: any) => {
            let job: any = list[key];
            jobs.push({
                key: key,
                jobCategoryId: job.jobCategoryId,
                dateCreated: job.dateCreated,
                user: {
                    uid: job.user.uid,
                    username: job.user.username
                },
                quotations: job.quotations == null ? 0 : job.quotations,
                jobDate: job.jobDate,
                jobTime: job.jobTime,
                pincode: job.pincode,
                jobStatus: job.jobStatus,
                city: job.city,
                chefGuestsNumber: job.chefGuestsNumber,
                chefEventType: job.eventType,
                cleanNoRooms: job.cleanNoRooms,
                cleanAddRooms: job.cleanAddRooms,
                customJob: job.customJob,
                washNoClothes: job.washNoClothes,
                washService: job.washService,
                recycleType: job.recycleType,
                paintNoWalls: job.paintNoWalls,
                paintMaterials: job.paintMaterials,
                fAssemblyNoUnits: job.fAssemblyNoUnits,
                fMoveNoUnits: job.fMoveNoUnits,
                fMoveDestAddress: job.fMoveDestAddress,
                quotation: {
                    message: job.quotation.message,
                    jobKey: job.quotation.jobKey,
                    price: job.quotation.price,
                    user: job.quotation.user,
                    dateCreated: job.quotation.dateCreated
                }
            });
        });
        return jobs;
    }

    getChats(snapshot: any): Array<IMessage> {
        let chats: Array<IMessage> = [];
        if (snapshot.val() == null)
            return chats;

        let list = snapshot.val();

        Object.keys(snapshot.val()).map((key: any) => {
            let chat: any = list[key];
            chats.push({
                key: key,
                message: chat.message,
                quotationKey: chat.quotationKey,
                user: chat.user,
                dateCreated: chat.dateCreated
            });
        });

        return chats;
    }

    getChat(snapshot: any, key: string): IMessage {
        let chat: IMessage = {
            key: key,
            message: snapshot.message,
            quotationKey: snapshot.quotationkey,
            user: snapshot.user,
            dateCreated: snapshot.dateCreated
        }
        return chat;
    }

    getRatings(snapshot: any): Array<IRating> {
        let ratings: Array<IRating> = [];
        if (snapshot.val() == null)
            return ratings;

        let list = snapshot.val();

        Object.keys(snapshot.val()).map((key: any) => {
            let rating: any = list[key];
            ratings.push({
                key: key,
                sellerId: rating.sellerId,
                pricing: rating.pricing,
                punctuality: rating.punctuality,
                quality: rating.quality,
                responsiveness: rating.respnsiveness,
                professionalism: rating.professionalism,
                job: {
                    jobCategoryId: rating.job.jobCategoryId,
                    dateCreated: rating.job.dateCreated,
                    user: {
                        uid: rating.job.user.uid,
                        username: rating.job.user.username
                    },
                    quotations: rating.job.quotations == null ? 0 : rating.job.quotations,
                    jobDate: rating.job.jobDate,
                    jobTime: rating.job.jobTime,
                    pincode: rating.job.pincode,
                    jobStatus: rating.job.jobStatus,
                    city: rating.job.city,
                    chefGuestsNumber: rating.job.chefGuestsNumber,
                    chefEventType: rating.job.eventType,
                    cleanNoRooms: rating.job.cleanNoRooms,
                    cleanAddRooms: rating.job.cleanAddRooms,
                    customJob: rating.job.customJob,
                    washNoClothes: rating.job.washNoClothes,
                    washService: rating.job.washService,
                    recycleType: rating.job.recycleType,
                    paintNoWalls: rating.job.paintNoWalls,
                    paintMaterials: rating.job.paintMaterials,
                    fAssemblyNoUnits: rating.job.fAssemblyNoUnits,
                    fMoveNoUnits: rating.job.fMoveNoUnits,
                    fMoveDestAddress: rating.job.fMoveDestAddress
                }
            });
        });
        return ratings;
    }

}
import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { IQuotation } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';
import { ItemsService } from '../../shared/services/items.service';
import { MappingsService } from '../../shared/services/mappings.service';
import { JobComponent } from '../../shared/components/job.component';

import { ChatPage } from '../chat/chat';


/*
  Generated class for the Chats page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html'
})
export class ChatsPage {
  public quotations: Array<IQuotation> = [];
  public newquotations: Array<IQuotation> = [];
  public loading: boolean = true;

  constructor(public navCtrl: NavController,
    public authService: AuthService,
    public dataService: DataService,
    public mappingsService: MappingsService,
    public itemsService: ItemsService,
    private events: Events) { }

  ionViewDidLoad() {
    console.log('Hello ChatsPage Page');
  }

  ngOnInit() {
    var self = this;
    self.loadChats(true);
    this.dataService.getUserQuotationsRef().child(self.authService.getLoggedInUser().uid).on('child_added', self.onChatAdded);
  }

  public onChatAdded = (childSnapshot, prevChildKey) => {
    var self = this;
    console.log('onChatAdded function Fired');
    self.events.publish('chat:created');
  }

  loadChats(fromStart: boolean) {
    var self = this;
    if (fromStart)
      self.loading = true;
    self.quotations = [];
    this.dataService.getUserQuotationsRef().child(self.authService.getLoggedInUser().uid).orderByChild('status').equalTo('open').once('value', function (snapshot) {
      self.itemsService.reversedItems<IQuotation>(self.mappingsService.getQuotations(snapshot)).forEach(function (quotation) {
        self.quotations.push(quotation);
        console.log(quotation);
        self.loading = false;
      });
      self.events.publish('chats:viewed');
    });
  }

  gotoChat(quotationKey: string, jobKey: string, sellerId: string) {
    this.navCtrl.push(ChatPage, { quotationKey: quotationKey, jobKey: jobKey, sellerId: sellerId });
  }

  reload(refresher) {
    this.loadChats(true);
    refresher.complete();
  }

}

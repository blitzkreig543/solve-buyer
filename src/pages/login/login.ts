import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

import { TabsPage } from '../tabs/tabs';
import { UserCredentials } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';
import { ProfileImageUploadPage } from '../profile-image-upload/profile-image-upload';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {

    loginFirebaseAccountForm: FormGroup;
    email: AbstractControl;
    password: AbstractControl;

    constructor(public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public fb: FormBuilder,
        public dataService: DataService,
        public authService: AuthService) { }

    ionViewDidLoad() {
        console.log('Hello LoginPage Page');
    }

    onLogin(form) {
        this.navCtrl.push(TabsPage);
    }

    ngOnInit() {
        this.loginFirebaseAccountForm = this.fb.group({
            'email': ['', Validators.compose([Validators.required])],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(5)])]
        });

        this.email = this.loginFirebaseAccountForm.controls['email'];
        this.password = this.loginFirebaseAccountForm.controls['password'];
    }

    onSubmit(signInForm: any): void {
        var self = this;
        if (this.loginFirebaseAccountForm.valid) {

            let loader = this.loadingCtrl.create({
                content: 'Logging In',
                dismissOnPageChange: true
            });

            loader.present();

            let user: UserCredentials = {
                email: signInForm.email,
                password: signInForm.password

            };

            console.log(user);
            this.authService.signInUser(user.email, user.password)
                .then(function (result) {
                    self.dataService.checkUserImage(self.authService.getLoggedInUser().uid).then(function (snapshot) {
                        var check = snapshot.hasChild("image");
                        console.log('value of check at login is ' + check);
                        if (!check) {
                            self.navCtrl.push(ProfileImageUploadPage);
                        } else {
                            self.navCtrl.setRoot(TabsPage);
                        }
                    });

                }).catch(function (error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    loader.dismiss().then(() => {
                        let toast = self.toastCtrl.create({
                            message: errorMessage,
                            duration: 4000,
                            position: 'top'
                        });
                        toast.present();
                    });
                });
        }
    }

    onLoginImageUpload() {
        this.navCtrl.push(ProfileImageUploadPage);
    }

}

import { Component, ViewChild, OnInit } from '@angular/core';
import { Platform, Nav, MenuController, ActionSheetController, ModalController, AlertController } from 'ionic-angular';
import { StatusBar, Splashscreen, Push } from 'ionic-native';

import { TutorialPage } from '../pages/tutorial/tutorial';
import { JobsPage } from '../pages/jobs/jobs';
import { BookedJobsPage } from '../pages/booked-jobs/booked-jobs';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { FeedbackPage } from '../pages/feedback/feedback';
import { UserProfilePage } from '../pages/user-profile/user-profile';
import { SocialSharing } from 'ionic-native';
import { AuthService } from '../shared/services/auth.service';
import { DataService } from '../shared/services/data.service';


export interface PageObj {
  title: string;
  component: any;
  icon: string;
}

@Component({
  templateUrl: `app.template.html`
})
export class MyApp implements OnInit {
  @ViewChild(Nav) nav: Nav;
  public device_id: any;

  appPages: PageObj[] = [
    { title: 'Home', component: HomePage, icon: 'home' },
    { title: 'All Jobs', component: JobsPage, icon: 'calendar' },
    { title: 'Booked Jobs', component: BookedJobsPage, icon: 'contacts' },
    { title: 'Profile', component: UserProfilePage, icon: 'contacts' },
    { title: 'Give Feedback', component: FeedbackPage, icon: 'calendar' }
  ];

  rootPage: any;
  TutorialPage: TutorialPage;

  constructor(platform: Platform,
    public menu: MenuController,
    private actionSheetCtrl: ActionSheetController,
    public authService: AuthService,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public dataService: DataService) {

    var self = this;
    this.rootPage = TabsPage;

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
      let push = Push.init({
        android: {
          senderID: "1081528584655"
        },
        ios: {
          alert: "true",
          badge: false,
          sound: "true"
        },
        windows: {}
      });

      push.on('registration', (data) => {

        console.log("device token ->", data.registrationId);
        this.device_id = data.registrationId;
        //TODO - send device token to server
      });
      push.on('notification', (data) => {
        console.log('message', data.message);
        let self = this;
        //if user using app and push notification comes
        if (data.additionalData.foreground) {
          // if application open, show popup
          let confirmAlert = this.alertCtrl.create({
            title: 'New Notification',
            message: data.message,
            buttons: [{
              text: 'Ignore',
              role: 'cancel'
            }, {
              text: 'View',
              handler: () => {
                //TODO: Your logic here
                self.nav.push(TabsPage, { message: data.message });
              }
            }]
          });
          confirmAlert.present();
        } else {
          //if user NOT using app and push notification comes
          //TODO: Your logic on click of push notification directly
          self.nav.push(TabsPage, { message: data.message });
          console.log("Push notification clicked");
        }
      });
      push.on('error', (e) => {
        console.log(e.message);
      });
    });
  }

  ngOnInit() {
    var self = this;
    this.authService.onAuthStateChanged(function (user) {
      if (user === null) {
        self.nav.setRoot(TutorialPage);
      }
    });
  }

  ngAfterViewInit() {

    var self = this;
    this.authService.onAuthStateChanged(function (user) {
      if (user === null) {
        self.menu.close();
        self.nav.setRoot(TutorialPage);
      } else {
        console.log("device token upon login is" + self.device_id);
        let uid = self.authService.getLoggedInUser().uid;
        console.log("uid at login is " + uid); 
        self.dataService.setDeviceToken(uid, self.device_id);
      }

    });
  }

  openPage(page: PageObj) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }
  showShareActions() {
    var self = this;
    let actionSheet = self.actionSheetCtrl.create({
      title: 'Share Job',
      buttons: [
        {
          text: 'WhatsApp',
          icon: 'logo-whatsapp',
          handler: () => {
            self.whatsappShare();
          }
        },
        {
          text: 'Twitter',
          icon: 'logo-twitter',
          handler: () => {
            self.twitterShare();
          }
        },
        {
          text: 'Facebook',
          icon: 'logo-facebook',
          handler: () => {
            self.facebookShare();
          }
        },
        {
          text: 'Other',
          icon: 'checkmark-circle',
          handler: () => {
            self.otherShare();
          }
        },
        {
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel',
          handler: () => { }
        }
      ]
    });

    actionSheet.present();
  }

  whatsappShare() {
    SocialSharing.shareViaWhatsApp("Found An Exciting job for you @solve", null /*Image*/, "http://solve.com/" /* url */)
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  twitterShare() {
    SocialSharing.shareViaTwitter("Message via Twitter", null /*Image*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  facebookShare() {
    SocialSharing.shareViaFacebook("Message via Twitter", null /*Image*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  otherShare() {
    SocialSharing.share("Genral Share Sheet", null/*Subject*/, null/*File*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })

  }

  showShareActions2() {
    var self = this;
    let actionSheet = self.actionSheetCtrl.create({
      title: 'Share Job',
      buttons: [
        {
          text: 'WhatsApp',
          icon: 'logo-whatsapp',
          handler: () => {
            self.whatsappShare2();
          }
        },
        {
          text: 'Twitter',
          icon: 'logo-twitter',
          handler: () => {
            self.twitterShare2();
          }
        },
        {
          text: 'Facebook',
          icon: 'logo-facebook',
          handler: () => {
            self.facebookShare2();
          }
        },
        {
          text: 'Other',
          icon: 'checkmark-circle',
          handler: () => {
            self.otherShare2();
          }
        },
        {
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel',
          handler: () => { }
        }
      ]
    });

    actionSheet.present();
  }

  whatsappShare2() {
    SocialSharing.shareViaWhatsApp("Found An Exciting App for you @solve", null /*Image*/, "http://solve.com/" /* url */)
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  twitterShare2() {
    SocialSharing.shareViaTwitter("Message via Twitter", null /*Image*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  facebookShare2() {
    SocialSharing.shareViaFacebook("Message via Twitter", null /*Image*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  otherShare2() {
    SocialSharing.share("Genral Share Sheet", null/*Subject*/, null/*File*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })

  }

  signout() {
    var self = this;
    self.menu.close();
    self.authService.signOut();
  }
}

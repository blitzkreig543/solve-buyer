import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PhotoViewer } from 'ionic-native';

import { IUser, IRating } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';
import { MappingsService } from '../../shared/services/mappings.service';
import { ItemsService } from '../../shared/services/items.service';

/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage implements OnInit {
  userDataLoaded: boolean = false;
  userId: string;
  userProfile = {};
  userDocument1 = {};
  userDocument2 = {};
  user: IUser;
  public ratings: Array<IRating> = [];
  document1Url: string;
  document2Url: string;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public dataService: DataService,
    public authService: AuthService,
    public mappingsService: MappingsService,
    public itemsService: ItemsService) { }

  ionViewDidLoad() {
    console.log('Hello ProfilePage Page');
  }

  ngOnInit() {
    var self = this;
    this.loadUserProfile();
    self.userId = self.navParams.get('userId');
    console.log('userId at profile page is ' + self.userId);

    self.dataService.getStorageRef().child('images/' + self.userId + '/document1.png').getDownloadURL().then(function (url) {
      self.document1Url = url.split('?')[0] + '?alt=media' + '&t=' + (new Date().getTime());
    });

    self.dataService.getStorageRef().child('images/' + self.userId + '/document2.png').getDownloadURL().then(function (url) {
      self.document2Url = url.split('?')[0] + '?alt=media' + '&t=' + (new Date().getTime());
    });
  }

  loadUserProfile() {
    var self = this;
    self.userDataLoaded = false;

    self.getUserData().then(function (snapshot) {
      let userData: any = snapshot.val();

      self.getUserImage().then(function (url) {
        self.userProfile = {
          username: userData.username,
          dateOfBirth: userData.dateOfBirth,
          country: userData.country,
          image: url
        };

        self.user = {
          uid: self.userId,
          username: userData.username
        };

        self.userDataLoaded = true;
      });

      self.getUserDocument1().then(function (url) {
        self.userDocument1 = {
          image: url
        }
      });
      self.getUserDocument2().then(function (url) {
        self.userDocument2 = {
          image: url
        }
      });
    });

    self.getUserRatings();
  }



  getUserData() {
    var self = this;
    self.userId = self.navParams.get('userId');
    console.log('value of userid at getuserdata function is ' + self.userId);
    return self.dataService.getUser(self.userId);

  }

  getUserImage() {
    var self = this;

    return self.dataService.getStorageRef().child('images/' + self.userId + '/profile.png').getDownloadURL();
  }

  getUserDocument1() {
    var self = this;

    return self.dataService.getStorageRef().child('images/' + self.userId + '/document1.png').getDownloadURL();
  }

  getUserDocument2() {
    var self = this;

    return self.dataService.getStorageRef().child('images/' + self.userId + '/document2.png').getDownloadURL();
  }

  getUserDocument3() {
    var self = this;

    return self.dataService.getStorageRef().child('images/' + self.userId + '/document3.png').getDownloadURL();
  }

  getUserRatings() {
    var self = this;
    self.userId = self.navParams.get('userId');
    console.log('id at user ratings is  ' + self.userId);
    this.dataService.getRatingsRef().orderByChild('sellerId').equalTo(self.userId).once('value', function (snapshot) {
      self.itemsService.reversedItems<IRating>(self.mappingsService.getRatings(snapshot)).forEach(function (rating) {
        self.ratings.push(rating);
      });
    });

  }

  zoom() {
    PhotoViewer.show(this.document1Url, 'Document 1', { share: false });
  }

   zoom2() {
    PhotoViewer.show(this.document2Url, 'Document 2', { share: false });
  }

}

import { Injectable } from '@angular/core';
import { IJob, IRating, IMessage } from '../interfaces';

declare var firebase: any;

@Injectable()
export class DataService {
    databaseRef: any = firebase.database();
    usersRef: any = firebase.database().ref('users');
    jobsRef: any = firebase.database().ref('jobsMaster');
    userJobsRef: any = firebase.database().ref('userjobs');
    BookedJobsRef: any = firebase.database().ref('bookedjobs');
    categoryJobsRef: any = firebase.database().ref('categoryJobs');
    storageRef: any = firebase.storage().ref();
    userQuotationsRef: any = firebase.database().ref('userQuotations');
    quotationsRef: any = firebase.database().ref('quotations');
    ratingsRef: any = firebase.database().ref('rating');
    chatsRef: any = firebase.database().ref('chats');

    connected: boolean = true;

    constructor() { }

    isFirebaseConnected() {
        return this.connected;
    }

    getUserJobsRef() {
        return this.userJobsRef;
    }

    getBookedJobsRef() {
        return this.BookedJobsRef;
    }

    getJobsRef() {
        return this.jobsRef;
    }

    getRatingsRef() {
        return this.ratingsRef;
    }

    getStorageRef() {
        return this.storageRef;
    }

    getUserQuotationsRef() {
        return this.userQuotationsRef;
    }

    getCategoryJobsRef() {
        return this.categoryJobsRef;
    }

    getUsername(userUid: string) {
        return this.usersRef.child(userUid + '/username').once('value');
    }

    getJobCategory(jobKey: string) {
        return this.jobsRef.child(jobKey).once('value');
    }

    getChatJobRef(jobKey: string, uid: string) {
        console.log('jobkey entering dataservice' + jobKey);
        return this.userJobsRef.child(uid).orderByKey().equalTo(jobKey).once('value');
    }

    getChatQuotationRef(quotationKey: string) {
        console.log('quotationkey recieved at dataservice' + quotationKey);
        return this.quotationsRef.orderByKey().equalTo(quotationKey);
    }

    getMessageRef(quotationKey: string) {
        return this.chatsRef.child(quotationKey);
    }

    submitJob(job: IJob, uid: string, category: string) {
        var newJobRef = this.jobsRef.push();
        var newJobKey = newJobRef.key;
        console.log(category);
        this.userJobsRef.child(uid).child(newJobKey).set(job);
        this.categoryJobsRef.child(category).child(newJobKey).set(job);
        return newJobRef.set(job);
    }

    submitRating(rating: IRating, jobKey: string) {
        var newChatRef = this.ratingsRef.push();
        return newChatRef.set(rating).then(() => {
            this.jobsRef.child(jobKey).once('value').then(function (snapshot) {
                newChatRef.child('job').update(snapshot.val())
            })
        });
    }

    submitChat(quotationKey: string, chats: IMessage) {
        this.chatsRef.child(quotationKey).child(chats.key).set(chats);
    }

    getQuotation(quotationKey: string) {
        return this.quotationsRef.child(quotationKey).once('value');
    }

    getJob(jobKey: string) {
        return this.jobsRef.child(jobKey).once('value');
    }

    checkUserImage(uid: string) {
        return this.usersRef.child(uid).once("value");
    }

    getUser(userUid: string) {
        return this.usersRef.child(userUid).once('value');
    }

    setUserImage(uid: string) {
        this.usersRef.child(uid).update({
            image: true
        });
    }

    setDeviceToken(uid: string, deviceToken: string) {
        console.log(" uid and device token at setdevice token is " + uid + ' > ' + deviceToken);
        this.usersRef.child(uid).child('deviceToken').set(deviceToken
        );
    }
}
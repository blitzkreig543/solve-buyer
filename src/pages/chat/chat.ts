import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ToastController, Events, Content } from 'ionic-angular';

import { IJob, IQuotation, IMessage, IUser } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';
import { MappingsService } from '../../shared/services/mappings.service';
import { ItemsService } from '../../shared/services/items.service';
import { SellerProfilePage } from '../seller-profile/seller-profile';
import { ProfilePage } from '../profile/profile';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

/*
  Generated class for the Chat page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})
export class ChatPage implements OnInit {
  @ViewChild(Content) content: Content;
  jobCategory: string;
  jobKey: string;
  quotationKey: string;
  sellerId: string;
  status: string;
  jobs: IJob[];
  quotations: IQuotation[];
  public chats: Array<IMessage> = [];
  public newChats: Array<IMessage> = [];
  chatForm: FormGroup;
  message: AbstractControl;
  public loading: boolean = true;


  constructor(public navCtrl: NavController,
    private navParams: NavParams,
    private actionSheeCtrl: ActionSheetController,
    private itemsService: ItemsService,
    private dataService: DataService,
    private authService: AuthService,
    private fb: FormBuilder,
    private mappingsService: MappingsService,
    private toastCtrl: ToastController,
    private events: Events) { }

  ionViewDidLoad() {
    console.log('Hello ChatPage Page');
  }

  ngOnInit() {
    var self = this;
    self.jobKey = self.navParams.get('jobKey');
    self.quotationKey = self.navParams.get('quotationKey');
    self.sellerId = self.navParams.get('sellerId');
    console.log('jobKey is' + this.jobKey, '>quotationKey' + this.quotationKey, 'seller id' + this.sellerId);

    self.dataService.getUsername(self.sellerId).then(function (snapshot) {
      let chatname = snapshot.val();
      document.getElementById("output").innerHTML = chatname;
    });

    self.dataService.getJobCategory(self.jobKey).then(function (snapshot) {
      self.jobCategory = snapshot.val().jobCategoryId;
      console.log('job category id at chat page is ' + self.jobCategory);
    })

    let uid = self.authService.getLoggedInUser().uid;
    self.dataService.getChatJobRef(self.jobKey, uid).then(function (snapshot) {
      self.status = snapshot.val().jobStatus;
      console.log(self.status);
      self.jobs = self.mappingsService.getJobs(snapshot);
    }, function (error) { });

    self.dataService.getChatQuotationRef(self.quotationKey).once('value', function (snapshot) {
      self.quotations = self.mappingsService.getQuotations(snapshot);
    }, function (error) { });

    this.chatForm = this.fb.group({
      'message': ['', Validators.compose([Validators.required, Validators.minLength(1)])]
    });

    this.message = this.chatForm.controls['message'];
    self.dataService.getMessageRef(self.quotationKey).once('value', function (snapshot) {
      self.chats = self.mappingsService.getChats(snapshot);
    }, function (error) { });

    self.dataService.getMessageRef(self.quotationKey).limitToLast(1).on('child_added', self.onChatAdded);
    self.events.subscribe('chats:add', self.addNewChats);

    self.loadChats(true);
  }

  public onChatAdded = (childSnapshot, prevChildKey) => {
    let xyz = childSnapshot.val().key;
    console.log('must be a key > childsnapshot value from onChatAdded' + xyz);
    var self = this;
    console.log('new chat added event fired');
    self.dataService.getMessageRef(self.quotationKey).orderByChild("key").equalTo(xyz).once('value').then(function (dataSnapshot) {
      let key = Object.keys(dataSnapshot.val())[0];
      console.log('value of first position' + key);
      let newChats: IMessage = self.mappingsService.getChat(dataSnapshot.val()[key], key);
      self.newChats.push(newChats);
      self.events.publish('chats:add');
    });
  }

  public addNewChats = () => {
    var self = this;
    self.newChats.forEach(function (chat: IMessage) {
      self.chats.push(chat);
    });

    self.newChats = [];
    self.scrollToBottom();
  }

  loadChats(fromStart: boolean) {
    var self = this;
    if (fromStart) {
      self.loading = true;
      self.chats = [];
      self.newChats = [];

      self.dataService.getMessageRef(self.quotationKey).once('value', function (snapshot) {
        self.chats = self.mappingsService.getChats(snapshot);
      }, function (error) { });
    }
    self.loading = false;
  }

  onSubmit(chatform: any): void {
    var self = this;

    let uid = self.authService.getLoggedInUser().uid;
    self.dataService.getUsername(uid).then(function (snapshot) {
      let username = snapshot.val();

      let chatRef = self.dataService.chatsRef.push();
      let chatkey: string = chatRef.key;
      console.log(chatkey);
      let user: IUser = { uid: uid, username: username };

      let newChat: IMessage = {
        key: chatkey,
        quotationKey: self.quotationKey,
        message: chatform.message,
        user: user,
        dateCreated: new Date().toString()
      };

      self.dataService.submitChat(self.quotationKey, newChat);
    });
  }

  showBookActions() {
    var self = this;
    let actionSheet = self.actionSheeCtrl.create({
      title: 'Confirm Booking',
      buttons: [
        {
          text: 'Book',
          icon: 'checkmark-circle',
          handler: () => {
            self.bookJob();
          }
        },
        {
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel',
          handler: () => { }
        }
      ]
    });

    actionSheet.present();
  }

  bookJob() {
    var self = this;
    let uid = self.authService.getLoggedInUser().uid;

    self.dataService.getUserJobsRef().child(self.authService.getLoggedInUser().uid).child(self.jobKey).update({
      jobStatus: 'booked'
    });
    self.dataService.getCategoryJobsRef().child(self.jobCategory).child(self.jobKey).update({
      jobStatus: 'booked'
    });
    self.dataService.getQuotation(self.quotationKey).then(function (snapshot) {
      self.dataService.getUserJobsRef().child(uid).child(self.jobKey + '/quotation/').set(snapshot.val());
    }).then(() => {
      self.dataService.getUserJobsRef().child(uid).child(self.jobKey).once('value').then(function (snapshot) {
        self.dataService.getBookedJobsRef().child(self.sellerId).child(self.jobKey).set(snapshot.val()).then(() => {

        });
      });
    }).then(() => {
      let toast = self.toastCtrl.create({
        message: 'Job Booked Succesfully',
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }



  gotoSellerProfile() {
    this.navCtrl.push(SellerProfilePage);
  }

  gotoUserProfilePage(userId : string) {
    this.navCtrl.push(ProfilePage, { userId : userId });
  }
      scrollToBottom() {
    var self = this;
    setTimeout(function () {
      self.content.scrollToBottom();
    }, 700);
  }

}

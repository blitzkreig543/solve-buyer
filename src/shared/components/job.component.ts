import { Component, EventEmitter, OnInit, OnDestroy, Input, Output } from '@angular/core';

import { IJob } from '../interfaces';
import { UserAvatarComponent } from '../../shared/components/user-avatar.component';
import { DataService } from '../services/data.service';

@Component({
    selector: 'all-job',
    templateUrl: 'job.component.html',
})

export class JobComponent {
    @Input() job: IJob;
    @Output() onViewQuotations = new EventEmitter<string>();

    constructor(private dataService: DataService,
    ) { }
}